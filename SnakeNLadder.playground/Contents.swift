//: Playground - noun: a place where people can play

import UIKit

//number of player

var playerCount :Int = 1
var player1Position :Int = 1
var player2Position :Int = 1
var player1 :String = "player1"
var player2 :String = "player2"
var player :String



var haveWinner : Bool = false
var round : Int = 0
var winnerPosition : Int = 50


func getPlayerPosition(player :String, playerPosition : Int) -> Int
{
    print("\(player) current position \(playerPosition)")
    return playerPosition
}

func snakeOrladder(var playerPosition :Int) ->Int
{
    switch (playerPosition)
    {
    case 10, 15, 20:
        playerPosition = moveDownifPosition(playerPosition, reverseCount:3)
        break
        
    case 5, 14, 18:
        playerPosition = moveUpifPosition(playerPosition, forwardCount:3)
        break
        
    default:
        break
    }
    
    return playerPosition
}



func moveDownifPosition (var playerPosition :Int, reverseCount :Int) ->Int
{
    //let reverseCount :Int = Int(arc4random_uniform(UInt32(playerPosition-1)))
    playerPosition = playerPosition - (reverseCount)
    
    print("Reverse \(reverseCount) ")
    print("Move DOWN to new position \(playerPosition)")
    
    return playerPosition
}

func moveUpifPosition (var playerPosition :Int, forwardCount :Int) ->Int
{
    //ladder
    //let forwardCount : Int = Int(arc4random_uniform(UInt32(playerPosition-1)))
    playerPosition = playerPosition + (forwardCount)
    print("Forward \(forwardCount) ")
    print("Move UP to new position \(playerPosition)")
    
    return playerPosition
}

func reverseIfExitBoard (var playerPosition : Int) -> Int
{
    //exitBoard
    print("Exist maximum position \(winnerPosition) now at position \(playerPosition)")
    //let reverseCount : Int = playerPosition - winnerPosition
    playerPosition = winnerPosition*2 - playerPosition
    print("exist and reverse to \(playerPosition)")
    
    return playerPosition
}

func rollDice() -> Int
{
    var rolldice : Int = Int(arc4random_uniform(6))
    
    rolldice = rolldice + 1;
    print("get dice count \(rolldice)")
    
    return rolldice
}

func turnsOfPlayer (player:String, var playerPosition :Int) ->Bool
{
    print("--- \(player) ---")
    getPlayerPosition(player, playerPosition: playerPosition)
    playerPosition += rollDice()
    getPlayerPosition(player, playerPosition: playerPosition)
    
    playerPosition = snakeOrladder(player1Position)
    getPlayerPosition(player, playerPosition: playerPosition)
    
    if (playerPosition > winnerPosition)
    {
        playerPosition = reverseIfExitBoard(playerPosition)
    }
    if (playerPosition == winnerPosition)
    {
        return true
    }
    return false
}



while (!haveWinner)
{
    round += 1
    print("\n### ROUND \(round) ###\n")
    
//    print("--- \(player1) ---")
//    getPlayerPosition(player1, playerPosition: player1Position)
//    player1Position += rollDice()
//    getPlayerPosition(player1, playerPosition: player1Position)
//    
//    player1Position = snakeOrladder(player1Position)
//    getPlayerPosition(player1, playerPosition: player1Position)
//    
//    if (player1Position > winnerPosition)
//    {
//        player1Position = reverseIfExitBoard(player1Position)
//    }
//    else if (player1Position == winnerPosition)
//    {
//        haveWinner = true
//        
//        print("\n\n*** Winner is player1! ***\n\n")
//        break
//    }
//    
//    print("\n--- \(player2) ---")
//    getPlayerPosition(player2, playerPosition: player2Position)
//    player2Position += rollDice()
//    getPlayerPosition(player2, playerPosition: player2Position)
//    
//    player2Position = snakeOrladder(player2Position)
//    getPlayerPosition(player2, playerPosition: player2Position)
//
//    if (player2Position > winnerPosition)
//    {
//        player2Position = reverseIfExitBoard(player2Position)
//    }
//    else if (player2Position == winnerPosition)
//    {
//        haveWinner = true
//        
//        print("\n\n*** Winner is player2! ***\n\n")
//        break
//    }
    
    
    
    
    
    

    
//    haveWinner = turnsOfPlayer(player1, playerPosition: player1Position)
//    
//    if (haveWinner && player == player1)
//    {
//        print("\n\n*** Winner is player1! ***\n\n")
//        break
//    }
//    
//    haveWinner = turnsOfPlayer(player2, playerPosition: player2Position)
//    if (haveWinner && player == player1)
//    {
//    print("\n\n*** Winner is player2! ***\n\n")
//    break
//    }

    
    print("\n")
}








